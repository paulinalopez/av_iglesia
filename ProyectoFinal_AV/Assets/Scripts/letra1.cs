﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class letra1 : MonoBehaviour
{
    //Para guardar las letras que ingrese el usuario
    public InputField field1;
    private string letraUno;
    public InputField field2;
    private string letraDos;
    public InputField field3;
    private string letraTres;
    public InputField field4;
    private string letraCuatro;
    public InputField field5;
    private string letraCinco;
    public InputField field6;
    private string letraSeis;
    public InputField field7;
    private string letraSiete;

    //Para mostrar que está incorrecta la respuesta
    public GameObject res2;

    //Para destruir el minijuego una vez ganado
    public GameObject puerta2;
    public GameObject trigger2;
    public GameObject panel2;

    //Para activar la tercera puerta
    public GameObject fpcPersonaje;
    private Personaje my_personaje;


    // Start is called before the first frame update
    void Start()
    {
        res2.SetActive(false);

        my_personaje = fpcPersonaje.GetComponent<Personaje>();

    }

    // Update is called once per frame
    void Update()
    {
    }

    public void OnSubmit()
    {
        letraUno = field1.text.ToUpper();
        letraDos = field2.text.ToUpper();
        letraTres = field3.text.ToUpper();
        letraCuatro = field4.text.ToUpper();
        letraCinco = field5.text.ToUpper();
        letraSeis = field6.text.ToUpper();
        letraSiete = field7.text.ToUpper();

        if (letraUno == "B" 
            && letraDos == "E" 
            && letraTres == "H" 
            && letraCuatro == "E" 
            && letraCinco == "M" 
            && letraSeis == "O" 
            && letraSiete == "T")
        {
            puerta2.SetActive(false);

            panel2.SetActive(false);
            Destroy(trigger2);

            my_personaje.miniGame2 = true;
        }

        else
        {
            res2.SetActive(true);
        }

    }

    public void Panel2()
    {
        panel2.SetActive(false);
    }
}
