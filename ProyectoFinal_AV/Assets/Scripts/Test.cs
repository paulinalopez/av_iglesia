﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public GameObject desaparece;

    // Start is called before the first frame update
    void Start()
    {
        desaparece.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        Verificar();
    }

    void OnTriggerEnter(Collider c)
    {
        desaparece.gameObject.SetActive(false);
    }

    void Verificar()
    {
        if (Input.GetKey(KeyCode.T))
        {
            desaparece.gameObject.SetActive(true);
        }
    }
}
