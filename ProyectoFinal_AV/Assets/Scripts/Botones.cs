﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class Botones : MonoBehaviour
{
    public GameObject pauseMenuUI;
    public GameObject pauseButton;

    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        pauseButton.SetActive(false);
        Time.timeScale = 0;
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        pauseButton.SetActive(true);
        Time.timeScale = 1;
    } 

    public void Escena1 (string nombredeescena)
    {
        SceneManager.LoadScene(1);
        
    }

    public void Escena2 (string nombredeescena)
    {
        SceneManager.LoadScene(2);
    }

    public void Escena3(string nombredeescena)
    {
        SceneManager.LoadScene(0);
    }

    public void Restart()
    {
        SceneManager.LoadScene("Iglesia");
        Time.timeScale = 1;
    }

    public void Fin()
    {
        Application.Quit();
        Debug.Break();
    }
}
