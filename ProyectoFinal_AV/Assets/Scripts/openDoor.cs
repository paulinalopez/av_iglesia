﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class openDoor : MonoBehaviour
{
    public GameObject puerta4;
    public GameObject trigger4;
    public GameObject panel4;

    // Start is called before the first frame update
    void Start()
    {
        puerta4 = GameObject.FindGameObjectWithTag("panelJuego4");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Yes()
    {
        SceneManager.LoadScene("Creditos");
        //puerta4.gameObject.SetActive(false);
        //Destroy(puerta4);
        Destroy(trigger4);
    }

    public void No()
    {
        panel4.SetActive(false);
    }
}
