﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPS : MonoBehaviour
{
    public Camera FPSCamera;
    public float speed;
    public Rigidbody rb;

   // public float horizontalSpeed;
   // public float verticalSpeed;
    //float hor;
    //float ver;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixdUpdate()
    {
       
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.MovePosition(movement * speed);

        //hor =horizontalSpeed * Input.GetAxis("Mouse X");
        //ver = verticalSpeed * Input.GetAxis("Mouse Y");
        //transform.Rotate(0, hor, 0);
        //FPSCamera.transform.Rotate(-ver,0,0);

        //float h = horizontalSpeed * Input.GetAxis("Horizontal");
        //float v = verticalSpeed * Input.GetAxis("Vertical");

        //transform.Translate(-h, 0, -v);
    }

    



}


