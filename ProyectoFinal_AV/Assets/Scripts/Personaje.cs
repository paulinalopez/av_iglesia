﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Personaje : MonoBehaviour
{
    
    //Para que suenen los items
    public AudioSource myAS;
    public AudioClip itemSound;

    //Para que aparezcan los minijuegos
    public GameObject panelJuego1;
    public GameObject panelJuego2;
    public GameObject panelJuego3;
    public GameObject panelJuego4;
    public Text panelTexto3;
    public GameObject llave1;
    public GameObject llave2;
    public GameObject llave3;
    public GameObject P_01;
    public GameObject P_t;

    private bool my_puerta1;
    public GameObject Prueba;

    //Para verificar que tiene todas las llaves
    public bool verLlave1;
    public bool verLlave2;
    public bool verLlave3;

    // Para activar las puertas
    public static bool miniGame1;
    public bool miniGame2;
    public bool miniGame3;

    //Para el contador de hojas
    public Text hojas;
    static int contador_hojas = 0;

    //Para el contador de llaves
    public GameObject panelLlave1;
    public GameObject panelLlave2;
    public GameObject panelLlave3;

    //Para eliminar el minijuego3 una vez ganado
    public GameObject puerta3;
    public GameObject trigger3;
    public bool contador;

    //Canvas Denied Access - activarlo y desactivarlo
    public GameObject CanvasDenied;

    void Start()
    {
        Prueba.SetActive(true);
        panelJuego1.SetActive(false);
        panelJuego2.SetActive(false);
        panelJuego3.SetActive(false);
        panelJuego4.SetActive(false);

        verLlave1 = false; 
        verLlave2 = false;
        verLlave3 = false;

        miniGame1 = GameController.pruebaLeo;
        miniGame2 = false; 
        miniGame3 = false; 

        llave1.SetActive(false);
        llave2.SetActive(false);
        llave3.SetActive(false);

        panelLlave1.SetActive(false);
        panelLlave2.SetActive(false);
        panelLlave3.SetActive(false);

        CanvasDenied.SetActive(false);
      
        contador = false;

        my_puerta1 = GameController.puerta1;
        P_01.SetActive(GameController.puerta1);
        P_t.SetActive(GameController.puerta1);
    }

    // Update is called once per frame
    void Update()
    {
        Verificar();
    }

   
    //Colision con puertas e items
    private void OnTriggerEnter(Collider c)
    {
        

        //Puerta1
        if (c.gameObject.tag == "Puerta01" && miniGame1 == false)
        {
            //P_01.SetActive(my_puerta1); se puede borrar
            GameObject.Destroy(P_t);
            
            SceneManager.LoadScene("Puzzle", LoadSceneMode.Single);
           // GameController gc = GameObject.Find("GameController").GetComponent<GameController>();
           
            //print(gc);

          //  if (gc!=null)
            //{
              
             
               // if (contador==true)
               // {
                  
                   // Prueba.SetActive(true);
                
               // }

                    
           // }

           //miniGame1 = true;
            //panelJuego1.SetActive(true);
        }

        //Hoja1
        if (c.gameObject.layer == 13)
        {
            contador_hojas += 1;
            string contador_string = contador_hojas.ToString();
            hojas.text = contador_string + "/3";
            Destroy(c.gameObject);
            myAS.PlayOneShot(itemSound);
        }

        //Puerta2
        if (c.gameObject.layer == 10)
        {
            if (miniGame1 == true)
            {
                panelJuego2.SetActive(true); 
            }

            else
            {
                //aparece un panel que te dice que no puedes acceder
                Debug.Log("Para acceder a esta puerta debes haber abierto la puerta 1");
                ActiveCanvasAcces();
            }
        }

        //Hoja2
        if (c.gameObject.layer == 14)
        {
            contador_hojas += 1;
            string contador_string = contador_hojas.ToString();
            hojas.text = contador_string + "/3";
            Destroy(c.gameObject);
            myAS.PlayOneShot(itemSound);
        }

        //Puerta3
        if (c.gameObject.layer == 11)
        {
            Debug.Log(miniGame2);

            if (miniGame2 == true && verLlave1 == false && verLlave2 == false && verLlave3 == false)
            {
                panelJuego3.SetActive(true);
                AparecenLlaves();
            }
            else if (miniGame2 == true && verLlave1 == true && verLlave2 == false && verLlave3 == false)
            {
                panelTexto3.text = "YOU MUST FIND ALL THE PIECES";
                panelJuego3.SetActive(true);
            }
            else if (miniGame2 == true && verLlave1 == true && verLlave2 == true && verLlave3 == false)
            {
                panelTexto3.text = "YOU MUST FIND ALL THE PIECES";
                panelJuego3.SetActive(true);
            }
            else if (miniGame2 == true && verLlave1 == true && verLlave2 == true && verLlave3 == true)
            {
                miniGame3 = true;
                Destroy(puerta3);
                Destroy(trigger3);
            }
            else
            {
                //aparece panel
                print("Para acceder a esta puerta debes haber abierto la puerta 2");
                ActiveCanvasAcces();
            }
        }

        //Llaves
        if (c.gameObject.layer == 16)
        {
            panelLlave1.SetActive(true);
            Destroy(c.gameObject);
            verLlave1 = true;
            llave2.SetActive(true);
            myAS.PlayOneShot(itemSound);
        }

        if (c.gameObject.layer == 17)
        {
            panelLlave2.SetActive(true);
            Destroy(c.gameObject);
            verLlave2 = true;
            llave3.SetActive(true);
            myAS.PlayOneShot(itemSound);
        }

        if (c.gameObject.layer == 18)
        {
            panelLlave3.SetActive(true);
            Destroy(c.gameObject);
            verLlave3 = true;
            myAS.PlayOneShot(itemSound);
        }

        //Hoja3
        if (c.gameObject.layer == 15)
        {
            contador_hojas += 1;
            string contador_string = contador_hojas.ToString();
            hojas.text = contador_string + "/3";
            Destroy(c.gameObject);
            myAS.PlayOneShot(itemSound);
        }

        //Puerta4
        if (c.gameObject.layer == 12)
        {
            if (miniGame3 == true)
            {
                panelJuego4.SetActive(true);
                
            }
            else
            {
                print("Para acceder a esta puerta debes haber abierto la puerta 3");
                ActiveCanvasAcces();
            }

        }
    }

    public void Verificar()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 1;
            panelJuego1.SetActive(false);
            panelJuego2.SetActive(false);
            panelJuego3.SetActive(false);
        }
    }

    public void AparecenLlaves()
    {
        llave1.SetActive(true);
    }

    public void ActiveCanvasAcces()
    {
        StartCoroutine("DeniedAccess");
    }

    IEnumerator DeniedAccess()
    {
        CanvasDenied.SetActive(true);
        yield return new WaitForSeconds(3f);
        CanvasDenied.SetActive(false);
    } 


}
