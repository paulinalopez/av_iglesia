﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hojas : MonoBehaviour
{
    public GameObject Pagina01;
    public GameObject Pagina02;
    public GameObject Pagina03;

    bool hoja01 = false;
    bool hoja02 = false;
    bool hoja03 = false;

    // Start is called before the first frame update
    void Start()
    {
        Pagina01.SetActive(false);
        Pagina02.SetActive(false);
        Pagina03.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        if (hoja01== true && Input.GetKeyDown(KeyCode.Escape))
        {
            
            Pagina01.SetActive(false);
        }

        if (hoja02==true && Input.GetKeyDown(KeyCode.Escape))
        {
            Pagina02.SetActive(false);
        }

        if (hoja03 == true && Input.GetKeyDown(KeyCode.Escape))
        {
            
            Pagina03.SetActive(false);
        }

    }

    void OnTriggerEnter(Collider collision)
    {
        if(collision.gameObject.tag=="Hoja01")
        {
            Pagina01.SetActive(true);
            hoja01 = true;
          
        }

        if (collision.gameObject.tag == "Hoja02")
        {
            Pagina02.SetActive(true);
            hoja02 = true;

        }

        if (collision.gameObject.tag == "Hoja03")
        {
            Pagina03.SetActive(true);
            hoja03 = true;

        }
    }
    
}
