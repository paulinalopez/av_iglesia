﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Creditos : MonoBehaviour
{
    RawImage video;
    AudioSource au;

    MovieTexture movie;
    AudioClip clip;

    public MovieTexture t;
    public AudioClip c;


    // Start is called before the first frame update
    void Start()
    {
        au = this.GetComponent<AudioSource>();
        video = this.GetComponent<RawImage>();
        ReproducirVideo(t, c);
    }

    void Update()
    {
        
    }

    public void ReproducirVideo(MovieTexture Movie, AudioClip Clip)
    {
        movie = Movie;
        clip = Clip;
        StartCoroutine("Reproducir");
    }
 
    IEnumerator Reproducir()
    {
        movie.Play();
        au.clip = clip;
        au.Play();
        yield return new WaitForSeconds(movie.duration + 1f);
        SceneManager.LoadScene("Menu");
    }
}
