﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public int level;
    public int fila, columna, countStep;
    public int filablank, columnablank;
    public int sizeFila, sizeColumna;
    int countPoint = 0;
    int countImageKey = 0;
    int countComp=0;

    public bool startControl = false;
    public bool checkComp;
    public bool gameisComplete;

    GameObject temp;

    public List<GameObject> checkPointList;
    public List<GameObject> imageOfPictureList;
    public List<GameObject> imageKeyList;
   

    GameObject[,] imageKeyMatrix;
    GameObject[,] imageOfPictureMatrix;
    GameObject[,] checkPointMatrix;

    public static bool pruebaLeo = false;
    public static bool puerta1 = true;

    // Start is called before the first frame update
    void Start()
    {
        gameisComplete = false;
        imageKeyMatrix = new GameObject[sizeFila,sizeColumna];
        imageOfPictureMatrix = new GameObject[sizeFila, sizeColumna];
        checkPointMatrix = new GameObject[sizeFila, sizeColumna];
        if (level==1)
        {
            ImageLevel();
        }
        CheckPointManager();
        ImageKeyManager();

        for (int f = 0; f < sizeFila; f++)
        {
            for (int c = 0; c < sizeColumna; c++)
            {
              if (imageOfPictureMatrix[f,c].name.CompareTo("blank")== 0)
                {
                    filablank = f;
                    columnablank = c;
                    break;
                }
               
            }
        }


    }

    void CheckPointManager()
    {
        for (int f=0; f<sizeFila; f++)
        {
            for (int c=0; c<sizeColumna; c++)
            {
                checkPointMatrix[f, c] = checkPointList[countPoint];
                countPoint++;
            }
        }
    }

    void ImageKeyManager()
    {
        for (int f = 0; f < sizeFila; f++)
        {
            for (int c = 0; c < sizeColumna; c++)
            {
                imageKeyMatrix[f, c] = imageKeyList[countImageKey];
                countImageKey++;
           
            }
        }
    }

    void Update()
    {
        if (startControl)
        {
            startControl = false;
            if (countStep == 1)
            {
                if (imageOfPictureMatrix[fila, columna]!=null && imageOfPictureMatrix[fila, columna].name.CompareTo("blank") != 0)
                {
                    if(filablank!=fila && columnablank==columna)
                    {
                        if(Mathf.Abs(fila-filablank)==1)
                        {
                            SortImage();
                            countStep = 0;
                        }
                        else
                        {
                            countStep = 0;
                        }
                       
                    }
                    else if (filablank==fila && columnablank != columna)
                    {
                        if (Mathf.Abs(columna - columnablank) == 1)
                        {
                            SortImage();
                            countStep = 0;
                        }

                        else
                        {
                            countStep = 0;
                        }


                    }
                    else if((filablank==fila && columnablank== columna) || (filablank!=fila && columnablank!=columna))
                    {
                        countStep = 0;
                    }

                    else
                    {
                        countStep = 0;
                    }
                }
            }
        }
    }

    void FixedUpdate()
    {
        if (checkComp)
        {
            checkComp = false;
            for(int f=0; f<sizeFila; f++)
            {
                for (int c= 0; c < sizeColumna; c++)
                {
                    if(imageKeyMatrix[f,c].gameObject.name.CompareTo(imageOfPictureMatrix[f,c].gameObject.name)==0)
                    {
                        countComp++;
                    }
                    else
                    {
                        break;
                    }
                }

            }

            if(countComp==checkPointList.Count)
            {
                gameisComplete = true;
                pruebaLeo = true;
                Personaje.miniGame1 = true;
                puerta1 = false;

                //pr.contador = true;
                Debug.Log("Game is OVER");
                SceneManager.LoadScene("Iglesia", LoadSceneMode.Single);
            }
            else
            {
                countComp = 0;  
            }
        }
    }

    void SortImage()
    {
        // sort
        temp = imageOfPictureMatrix[filablank,columnablank];
        imageOfPictureMatrix[filablank, columnablank] = null;

        imageOfPictureMatrix[filablank, columnablank] = imageOfPictureMatrix[fila, columna];
        imageOfPictureMatrix[fila, columna] = null;

        imageOfPictureMatrix[fila, columna] = temp;

        //move
        imageOfPictureMatrix[filablank, columnablank].GetComponent<ImageController>().target= checkPointMatrix[filablank, columnablank];
        imageOfPictureMatrix[fila, columna].GetComponent<ImageController>().target = checkPointMatrix[fila, columna];

        imageOfPictureMatrix[filablank, columnablank].GetComponent<ImageController>().startMove= true;
        imageOfPictureMatrix[fila, columna].GetComponent<ImageController>().startMove = true;

        filablank = fila;
        columnablank = columna;

    }

   void ImageLevel()
    {
        imageOfPictureMatrix[0, 0] = imageOfPictureList[0];
        imageOfPictureMatrix[0, 1] = imageOfPictureList[2];
        imageOfPictureMatrix[0, 2] = imageOfPictureList[5];
        imageOfPictureMatrix[1, 0] = imageOfPictureList[4];
        imageOfPictureMatrix[1, 1] = imageOfPictureList[1];
        imageOfPictureMatrix[1, 2] = imageOfPictureList[7];
        imageOfPictureMatrix[2, 0] = imageOfPictureList[3];
        imageOfPictureMatrix[2, 1] = imageOfPictureList[6];
        imageOfPictureMatrix[2, 2] = imageOfPictureList[8];
        
    }
}
